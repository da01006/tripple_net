import json
import os
import numpy as np
from tqdm import tqdm
import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader
from matplotlib import pyplot as plt
import torchvision
from torchvision import transforms as T

from torch.utils.tensorboard import SummaryWriter
from datetime import datetime

from dataloaders.hyper_dataset import HyperImageDataSet
from utils.img_util import HyperImageUtility
from loss_functions.retrieval_loss import NTXentLoss
from dataloaders.hyper_image_slicer_ds import HyperImagePatcherDataSet

from model.vgg_pl import Vgg16Main
torch.cuda.empty_cache()

# config = json.load(open("/vol/research/rssfeed/pytorch_impl/config/config.json"))
data_dir = r'/vol/research/RobotFarming/Projects/data/full'
experiment_name = 'tripplet_net'
experiment_dir = r'/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor'
log_dir = r'/vol/research/RobotFarming/Projects/tripplet_net/tb_logs'

def matplotlib_imshow(img, one_channel=False):
    if one_channel:
        img = img.mean(dim=0)
    img = img / 2 + 0.5     # unnormalize
    npimg = img.numpy()
    if one_channel:
        plt.imshow(npimg, cmap="Greys")
    else:
        plt.imshow(np.transpose(npimg, (1, 2, 0)))


def tb_add_images(anchor, positives):
    img_util = HyperImageUtility()
    a = img_util.move_axis(anchor[0].numpy())
    p = img_util.move_axis(positives[0].numpy())
    n = img_util.move_axis(positives[1].numpy())

    a = img_util.extract_percentile_range(a, 2, 98)
    p = img_util.extract_percentile_range(p, 2, 98)
    n = img_util.extract_percentile_range(n, 2, 98)

    a = img_util.extract_rgb(data=a, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
    p = img_util.extract_rgb(data=p, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
    n = img_util.extract_rgb(data=n, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))

    imgs = [torch.from_numpy(a), torch.from_numpy(p), torch.from_numpy(n)]
    img_grid = torchvision.utils.make_grid(imgs)
    # matplotlib_imshow(img_grid, one_channel=True)
    writer.add_image('tripplet images', img_grid)


batch_size = 10
in_channels=88
out_features=4096
learning_rate=0.00001
num_workers=0
# step_size=256
# num_patches=50
patch_height: int = 160
patch_width: int = 160
overlap_height_ratio: float = 0.05
overlap_width_ratio: float = 0.05

training_set = HyperImagePatcherDataSet(root_dir=data_dir,
                                        patch_height=patch_height,
                                        patch_width=patch_width,
                                        overlap_height_ratio=overlap_height_ratio,
                                        overlap_width_ratio=overlap_width_ratio,
                                        num_channels=in_channels, 
                                        apply_augmentation=True,
                                        is_train=True,
                                    )
training_loader = DataLoader(training_set, batch_size=batch_size, num_workers=num_workers)

validation_set = HyperImagePatcherDataSet(root_dir=data_dir,
                                        patch_height=patch_height,
                                        patch_width=patch_width,
                                        overlap_height_ratio=overlap_height_ratio,
                                        overlap_width_ratio=overlap_width_ratio,
                                        num_channels=in_channels, 
                                        apply_augmentation=True,
                                        is_train=False,
                                    )
validation_loader = DataLoader(validation_set, batch_size=batch_size, num_workers=num_workers)

# Report split sizes
print('Training set has {} instances'.format(len(training_set)))
print('Validation set has {} instances'.format(len(validation_set)))

model = Vgg16Main(in_channels=in_channels, out_features=out_features)
# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# model = model.to(device)

loss_fn = NTXentLoss()
optimizer = torch.optim.Adam(params=model.parameters(), lr=learning_rate)

def train_one_epoch(epoch_index, tb_writer):
    running_loss = 0.
    last_loss = 0.
    count = 0.0
    with tqdm(training_loader, unit='batch') as tepoch:
        for i, data in enumerate(tepoch):
            tepoch.set_description(f'Epoch {epoch_index + 1}')
            # print(f'loader len {len(training_loader)}')
            anchor, positives = data

            # zero gradients for every batch
            optimizer.zero_grad()

            #make predictions for this batch
            anchor = anchor.squeeze().float()
            positives = positives.squeeze().float()
            
            a_output = model(anchor)
            p_output = model(positives)
            # normalize => l2 norm
            a_output = F.normalize(a_output)
            p_output = F.normalize(p_output)

            # compute the loss and its gradients
            loss = loss_fn(a_output, p_output)
            loss.backward()

            # adjust learning weights
            optimizer.step()

            # make report
            running_loss += loss.item()
            count += 1.0
            # print(f'Train running loss: {running_loss}')
            loader_len = len(training_loader)
            # print(i)
            if i % 10 == 0 and i != 0:
                last_loss = running_loss / count
                # print(f'  batch loss: {last_loss}, batch size {count}')
                tb_x = epoch_index * loader_len + i + 1
                tb_writer.add_scalar('Loss/train', last_loss, tb_x)
                running_loss = 0.
                count = 1.
                tepoch.set_postfix(loss=last_loss)
                # tb_add_images(anchor, positives)

            tepoch.set_postfix(loss=loss.item())


    return last_loss


# Initializing in a separate cell so we can easily add more epochs to the same run
timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
writer = SummaryWriter(os.path.join(log_dir, 'tripplet_net_hw160_o005_b10_fext3_man_{}'.format(timestamp)))
epoch_number = 0

EPOCHS = 1000

best_vloss = 1_000_000.

for epoch in range(EPOCHS):
    # print('EPOCH {}:'.format(epoch_number + 1))

    # Make sure gradient tracking is on, and do a pass over the data
    model.train(True)
    avg_loss = train_one_epoch(epoch_number, writer)

    # We don't need gradients on to do reporting
    model.eval()

    running_vloss = 0.0
    for i, vdata in enumerate(validation_loader):
        vanchor, vpositives = vdata
        vanchor = vanchor.squeeze().float()
        vpositives = vpositives.squeeze().float()
        
        va_output = model(vanchor)
        vp_output = model(vpositives)
        # normalize => l2 norm
        va_output = F.normalize(va_output)
        vp_output = F.normalize(vp_output)

        # compute the loss and its gradients
        vloss = loss_fn(va_output, vp_output)
        running_vloss += vloss

    avg_vloss = running_vloss / (i + 1)
    print('LOSS train {} valid {}'.format(avg_loss, avg_vloss))

    # Log the running loss averaged per batch
    # for both training and validation
    writer.add_scalars('Training vs. Validation Loss',
                    { 'Training' : avg_loss, 'Validation' : avg_vloss },
                    epoch_number + 1)
    writer.flush()

    # Track best performance, and save the model's state
    if avg_vloss < best_vloss:
        best_vloss = avg_vloss
        model_path = os.path.join(experiment_dir, 'model_hw160_o005_b10_fext3_man_{}_{}'.format(timestamp, epoch_number) + '.pth')
        torch.save(model.state_dict(), model_path)

    epoch_number += 1


# if __name__ == '__main__':
#     dataiter = iter(training_loader)
#     raw, a, a_hat = dataiter.next()

#     print(len(dataiter))