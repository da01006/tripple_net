from collections import defaultdict
# import random

import torch
# import numpy as np
from torch import nn, optim #, triplet_margin_loss
import torch.nn.functional as F
import torchvision
# from vgg_pl import Vgg16Main

class Vgg16Net(torch.nn.Module):
    
    def __init__(self, in_channels=88, out_features=4096):
        super(Vgg16Net, self).__init__()
        self.in_channels = in_channels
        self.out_features = out_features
        # self.model = Vgg16Main(in_channels=self.in_channels, out_features=self.out_features)
        

    def training_step(self, batch, batch_idx):
        anchor, positives = batch

        anchor = anchor.squeeze().float()
        positives = positives.squeeze().float()
        # normalize => l2 norm
        anchor = F.normalize(anchor)# add an epsilon
        positives = F.normalize(positives)
        # print(f'anchor input {anchor.shape}, max: {torch.max(anchor)}')

        a_output = self.model(anchor)
        p_output = self.model(positives)

        loss, _, _ = self.loss(a_output, p_output)
        
        train_dict = defaultdict()
        if self.current_epoch % 5 == 0:
            train_dict = {
                'a': anchor,
                'p': positives,
            }
        return {'loss': loss.mean(), **train_dict}


    def validation_step(self, batch, batch_idx):
        anchor, positives = batch

        anchor = anchor.squeeze().float()
        positives = positives.squeeze().float()
        # normalize => l2 norm
        anchor = F.normalize(anchor)# add an epsilon
        positives = F.normalize(positives)

        a_output = self.model(anchor)
        p_output = self.model(positives)

        loss, _, _ = self.loss(a_output, p_output)
        self.log('Validation Loss', loss.mean(), prog_bar=True)

        return {'Validation Loss': loss.mean()}

    def training_epoch_end(self, outputs):
        if self.current_epoch == 0:
            sample_img = torch.rand((1, self.in_channels, 256, 256))
            self.logger.experiment.add_graph(self, sample_img.to(self.device))

        if self.current_epoch % 5 == 0:
            avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
            self.logger.experiment.add_scalar('Train Loss', avg_loss, self.current_epoch)

        # if self.current_epoch % 5 == 0:
        #     patch_size = 256
        #     channel = 88
        #     # log images

        #     # img_lst = list(range(5))
        #     # img_idx = random.choice(img_lst)
        #     img_idx = 0
        #     anchor = outputs[img_idx]['a']
        #     positive = outputs[img_idx]['p']

        #     self.tb_tripplet_image_adder(patch_size, channel, anchor.squeeze(), positive.squeeze())


    def validation_epoch_end(self, outputs):
        if self.current_epoch % 5 == 0:    
            avg_val_loss = torch.stack([x['Validation Loss'] for x in outputs]).mean()
            self.logger.experiment.add_scalar('Validation Loss', avg_val_loss, self.current_epoch)


    def tb_tripplet_image_adder(self, patch_size, channel, anchor, positive):
        anchor, unfold_shape = self.make_patches(anchor, patch_size=patch_size)

        anchor = anchor.view(unfold_shape).permute(0, 1, 4, 2, 5, 3, 6).contiguous()
        anchor = anchor.contiguous().view(-1, channel, patch_size, patch_size)

        positive = positive.view(unfold_shape).permute(0, 1, 4, 2, 5, 3, 6).contiguous()
        positive = positive.contiguous().view(-1, channel, patch_size, patch_size)
            
        ref = torch.squeeze(anchor.view(-1, anchor.shape[-2], anchor.shape[-1]), 1)
        positive = torch.squeeze(positive.view(-1, positive.shape[-2], positive.shape[-1]), 1)
            
        ref = self.extract_rgb(data=ref, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
        positive = self.extract_rgb(data=positive, r_range=(36, 38), g_range=(13, 15), b_range=(10, 12))

        imgs = [ref, positive]
        imgs = torchvision.utils.make_grid(imgs)

        self.logger.experiment.add_image('Images', imgs, self.current_epoch)

    def make_patches(self, x, patch_size):
        if x.dim()>3:
            channel_dim = x.shape[1]
            patches = x.unfold(1, channel_dim, channel_dim).unfold(2, patch_size, patch_size).unfold(3, patch_size, patch_size)
            unfold_shape = patches.size()
            patches = patches.contiguous().view(-1, channel_dim, patch_size, patch_size)
        else:
            patches = x.unfold(2, patch_size, patch_size).unfold(3, patch_size, patch_size)
            unfold_shape = patches.size()
            patches = patches.contiguous().view(-1, patch_size, patch_size)
        return patches, unfold_shape

 