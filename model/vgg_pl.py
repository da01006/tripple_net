from collections import defaultdict
import random

import torch
import numpy as np
from torch import nn, optim, triplet_margin_loss
import torch.nn.functional as F
import torchvision
# from torchgeo.datasets import stack_samples, unbind_samples
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from sklearn.decomposition import PCA 
from loss_functions.retrieval_loss import SimilarityLoss, ContrastiveLoss, NTXentLoss


class Vgg16Main(nn.Module):
    
    def __init__(self, in_channels=88, out_features=4096):
        super(Vgg16Main, self).__init__()
        self.in_channels = in_channels
        self.out_features = out_features
        # self.loss = NTXentLoss()
        
        self.block1 = nn.Sequential(
            nn.Conv2d(in_channels=self.in_channels, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.block2 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.block3 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=256, out_channels=256, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.block4 = nn.Sequential(
            nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.block5 = nn.Sequential(
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=512, out_channels=512, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(512),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2, stride=2)
        )

        self.fextractor = nn.Sequential(
            nn.Linear(in_features=12800, out_features=8192),
            nn.ReLU(inplace=True),
            nn.Dropout(p=0.65),
            nn.Linear(in_features=8192, out_features=self.out_features),
        )


    def forward(self, x):
        # x = self.shrink_channel_conv_layer(x)
        # print(f'x shape b4 block 1: {x.shape}')
        x = self.block1(x)
        # print(f'x shape b4 block 2: {x.shape}')
        x = self.block2(x)
        # print(f'x shape b4 block 3: {x.shape}')
        x = self.block3(x)
        # print(f'x shape b4 block 4: {x.shape}')
        x = self.block4(x)
        # print(f'x shape b4 block 5: {x.shape}')
        x = self.block5(x)
        # print(f'x shape b4 view: {x.shape}')
        x = x.view(x.size(0), -1)
        # print(f'x shape b4 classifier: {x.shape}')
        logits = self.fextractor(x)
        # probas = F.softmax(logits, dim=1)

        return logits



class Vgg16Net_PL(pl.LightningModule):
    
    def __init__(self, in_channels, out_features, learning_rate):
        super(Vgg16Net_PL, self).__init__()
        self.in_channels = in_channels
        self.out_features = out_features
        self.learning_rate = learning_rate
        
        self.loss = NTXentLoss()
        self.model = Vgg16Main(in_channels = in_channels, out_features = out_features)

    def training_step(self, batch, batch_idx):
        anchor, positives = batch

        anchor = anchor.squeeze().float()
        positives = positives.squeeze().float()
        # normalize => l2 norm
        anchor = F.normalize(anchor)# add an epsilon
        positives = F.normalize(positives)

        a_output = self.model(anchor)
        p_output = self.model(positives)

        loss = self.loss(a_output, p_output)
        
        return {'loss': loss.mean()}


    def validation_step(self, batch, batch_idx):
        anchor, positives = batch

        anchor = anchor.squeeze().float()
        positives = positives.squeeze().float()
        # normalize => l2 norm
        anchor = F.normalize(anchor)# add an epsilon
        positives = F.normalize(positives)

        a_output = self.model(anchor)
        p_output = self.model(positives)

        loss = self.loss(a_output, p_output)
        self.log('Validation Loss', loss.mean(), prog_bar=True)

        return {'Validation Loss': loss.mean()}


    def test_step(self, batch, batch_idx):
        pass


    def configure_optimizers(self):
        params = self.parameters()
        optimizer = optim.Adam(params=params, lr=self.learning_rate)
        return optimizer

    def training_epoch_end(self, outputs):
        # if self.current_epoch % 10 == 0 and self.current_epoch!= 0:
        print(outputs)
        avg_loss = torch.stack([x['loss'] for x in outputs]).mean()
        self.logger.experiment.add_scalar('Train Loss', avg_loss, self.current_epoch)


    def validation_epoch_end(self, outputs):
        # if self.current_epoch % 10 == 0 and self.current_epoch!= 0:    
        avg_val_loss = torch.stack([x['Validation Loss'] for x in outputs]).mean()
        self.logger.experiment.add_scalar('Val Loss', avg_val_loss, self.current_epoch)



def test():
    from dataloaders.hyper_image_slicer_ds import HyperImagePatcherDataSet

    img_root_dir = r'/vol/research/RobotFarming/Projects/data/full'
    num_channels: int = 88
    hip = HyperImagePatcherDataSet(img_root_dir, patch_height=160, 
                                    patch_width=160, 
                                    overlap_height_ratio=0.05, 
                                    overlap_width_ratio=0.05, 
                                    apply_augmentation=True, 
                                    is_train=True)

    from torch.utils.data import DataLoader
    print("Loaded Images with {} patches".format(len(hip)))
    training_loader = DataLoader(hip, batch_size=5, num_workers=16)
    model = Vgg16Main()#.to("cuda:0")
    data  = next(iter(training_loader))
    loss_fn = NTXentLoss()
    #loss_fn = torch.nn.SmoothL1Loss()

    optimizer = torch.optim.Adam(params=model.parameters(), lr=0.0001)

    for i in range(1000):
        a, p = data
        fa = model(a.squeeze())
        fp = model(p.squeeze())
        # fa = model(a.squeeze().to("cuda:0"))
        # fp = model(p.squeeze().to("cuda:0"))

        fa = F.normalize(fa)
        fp = F.normalize(fp)

        # compute the loss and its gradients
        loss = loss_fn(fa, fp)
        #loss = loss_fn(fa,fp)
        loss.backward()

        print(loss.item())

        # adjust learning weights
        optimizer.step()

    # for data in training_loader:
    #     a, p = data
    #     fa = model(a.squeeze().to("cuda:0"))
    #     fp = model(p.squeeze().to("cuda:0"))

    #     fa = F.normalize(fa)
    #     fp = F.normalize(fp)

    #     # compute the loss and its gradients
    #     loss = loss_fn(fa, fp)
    #     #loss = loss_fn(fa,fp)
    #     loss.backward()

    #     print(loss.item())

    #     # adjust learning weights
    #     optimizer.step()

        

    

if __name__== "__main__":
    # export PYTHONPATH='/vol/research/RobotFarming/Projects/tripplet_net'
    test()


