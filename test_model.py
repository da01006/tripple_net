import os
import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.data import DataLoader

from model.vgg import Vgg16Net
from dataloaders.hyper_dataset import HyperImageDataSet2
from utils.img_util import HyperImageUtility

# Specify a path
path = r"/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor/model_s256_20221115_121134_86.pth"
data_dir = r'/vol/research/RobotFarming/Projects/data/full'
project_root_dir = r'/vol/research/RobotFarming/Projects/tripplet_net'

batch_size = 1
# in_channels=88
# num_classes=128
# learning_rate=0.0001
num_workers=0
step_size=256
num_patches=50

training_set = HyperImageDataSet2(root_dir=data_dir, is_train=True, apply_augmentation=True, step_size=step_size, num_patches=num_patches)
training_loader = DataLoader(training_set, batch_size=batch_size, num_workers=num_workers)

img_util = HyperImageUtility()
# Load
model = Vgg16Net(in_channels=88, num_classes=32)
model.load_state_dict(torch.load(path))
model.eval()
# model.classifier = nn.Sequential(*list(model.classifier.children())[:-3])
print(list(model.classifier.children())[:-1])

# with torch.no_grad():
#     # sample = torch.rand(4,88,128,128)
#     # x = model(sample)
#     # print(x)
#     for param in model.parameters():
#         # print(param)
#         param.requires_grad = False
#         # print(param)
#         break

#     for i, data in enumerate(training_loader):
#         _, img_path, anchor, _ = data
#         # query = anchor[0][0].unsqueeze(0)
#         # print(query.shape)

#         # save image patches
#         # raw_img_fnames = img_util.save_image_patches(anchor, path=os.path.join(project_root_dir, r'/img_patches/raw'))
#         #make predictions for this batch
#         anchor = anchor.squeeze().float()
        
#         a_output = model(anchor)
#         # q_output = model(query)
#         # normalize => l2 norm
#         a_output = F.normalize(a_output)
#         # q_output = F.normalize(q_output[0])
#         query = a_output[0].unsqueeze(0)

#         sim = query * a_output
#         sim = sim.sum(1)
#         sim_rank = sim.argsort(descending=True)

#         # save feature patches
#         # feat_fnames = img_util.save_image_patches(a_output, path=os.path.join(project_root_dir, r'/img_patches/features'))

#         print(a_output.shape)
#         print(query.shape)
#         print(sim.shape)
#         print(img_path)
#         print(sim_rank)
#         print(query)
#         print(a_output[sim_rank[3].item()])
#         break
