import os
import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.data import DataLoader

from model.vgg import Vgg16Net
from model.vgg_pl import Vgg16Main
from dataloaders.hyper_dataset import HyperImageDataSet2
from utils.img_util import HyperImageUtility

# Specify a path
# path = r"/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor/model_s256_20221115_121134_86.pth"
path = r"/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor/model_hw160_o005_b10_fext3_man_20221130_175210_5.pth"
data_dir = r'/vol/research/RobotFarming/Projects/data/full'
project_root_dir = r'/vol/research/RobotFarming/Projects/tripplet_net'

batch_size = 1
# in_channels=88
# num_classes=128
# learning_rate=0.0001
num_workers=0
step_size=20
num_patches=50

val_set = HyperImageDataSet2(
                        root_dir=data_dir, 
                        is_train=False, 
                        apply_augmentation=True, 
                        step_size=step_size, 
                        num_patches=num_patches,
                        height=160,
                        width=160
                    )
val_loader = DataLoader(val_set, batch_size=batch_size, num_workers=num_workers)

img_util = HyperImageUtility()
# Load
# model = Vgg16Net(in_channels=88, num_classes=32)
model = Vgg16Main()
model.load_state_dict(torch.load(path))
# model.classifier = nn.Sequential(*list(model.classifier.children())[:-3])
model.eval()

with torch.no_grad():
    # sample = torch.rand(4,88,128,128)
    # x = model(sample)
    # print(x)
    for param in model.parameters():
        # print(param)
        param.requires_grad = False
        # print(param)
        # break

    feature_list = []
    patch_list = []
    for i, data in enumerate(val_loader):
        _, img_path, anchor, positives = data
        # query = positives[0][0].unsqueeze(0)
        # print(query.shape)
        patch_list.append(anchor)
        # patch_list.append(positives)
        
        #make predictions for this batch
        # anchor = anchor.squeeze().float()
        # positives = positives.squeeze().float()
        
        # a_output = model(anchor)
        # p_output = model(positives)
        # q_output = model(query.float())
        # normalize => l2 norm
        # a_output = F.normalize(a_output)
        # p_output = F.normalize(p_output)
        # q_output = F.normalize(q_output)

        # feature_list.append(a_output)

        # sim = q_output * a_output
        # sim = sim.sum(1)
        # sim_rank = sim.argsort(descending=True)

        # print(a_output.shape)
        # print(query.shape)
        # print(sim.shape)
        # print(img_path)
        # print(sim_rank)
        # print(q_output)
        # print(a_output[sim_rank[0].item()])
        # break
    
    # features = torch.cat(feature_list, dim=0)
    # feature_path=os.path.join(project_root_dir, r'img_patches/val/features')
    # # img_util.ensure_dir(feature_path)
    # shape = list(features.shape)
    # torch.save(features, os.path.join(feature_path, f'img_s20_feature_fext3_man_20221130_175210_5_{"x".join([str(x) for x in shape])}.pt'))
    # torch.save(features, os.path.join(feature_path, f'augmented_img_s20_feature_fext3_man_20221130_175210_5_{"x".join([str(x) for x in shape])}.pt'))

    # save image patches
    patches = torch.cat(patch_list, dim=1).squeeze()
    print(patches.shape)
    patch_path=r'/vol/research/RobotFarming/Projects/tripplet_net/img_patches/val/raw_img_s20_feature_fext3_man_20221130_175210_5'
    img_util.ensure_dir(patch_path)
    shape = list(patches.shape)
    own_filename=os.path.join(patch_path, f'raw_patch_s20_fext3_man_20221130_175210_5_{"x".join([str(x) for x in shape])}')
    print(own_filename)
    raw_img_fnames = img_util.save_image_patches(patches.numpy(), own_filename=own_filename)

    