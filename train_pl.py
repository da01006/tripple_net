import json
import torch
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from torch.utils.data import DataLoader
from matplotlib import pyplot as plt
# from util import is_similar_image
import torchvision.transforms as T
from pytorch_lightning.callbacks import EarlyStopping, ModelCheckpoint
from pytorch_lightning.utilities import fetching

from dataloaders.hyper_image_slicer_ds import HyperImagePatcherDataSet
from model.vgg_pl import Vgg16Net_PL
torch.cuda.empty_cache()

# config = json.load(open("/vol/research/rssfeed/pytorch_impl/config/config.json"))
data_dir = r'/vol/research/RobotFarming/Projects/data/full'
experiment_dir = r'/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor'
log_dir = r'/vol/research/RobotFarming/Projects/tripplet_net/tb_logs'

experiment_name = 'tripplet_net_hw160_o02_b10_fext_'
batch_size = 10
in_channels=88
out_features=4096
learning_rate=0.00001
num_workers=16
patch_height: int = 160
patch_width: int = 160
overlap_height_ratio: float = 0.2
overlap_width_ratio: float = 0.2

training_set = HyperImagePatcherDataSet(root_dir=data_dir,
                                        patch_height=patch_height,
                                        patch_width=patch_width,
                                        overlap_height_ratio=overlap_height_ratio,
                                        overlap_width_ratio=overlap_width_ratio,
                                        num_channels=in_channels, 
                                        apply_augmentation=True,
                                        is_train=True,
                                    )

training_loader = DataLoader(training_set, batch_size=batch_size, num_workers=num_workers)

validation_set = HyperImagePatcherDataSet(root_dir=data_dir,
                                        patch_height=patch_height,
                                        patch_width=patch_width,
                                        overlap_height_ratio=overlap_height_ratio,
                                        overlap_width_ratio=overlap_width_ratio,
                                        num_channels=in_channels, 
                                        apply_augmentation=True,
                                        is_train=False,
                                    )
validation_loader = DataLoader(validation_set, batch_size=batch_size, num_workers=num_workers)
# Report split sizes
print('Training set has {} instances'.format(len(training_set)))
print('Validation set has {} instances'.format(len(validation_set)))
print(f'Experiment: {experiment_name}')

model = Vgg16Net_PL(in_channels=in_channels, out_features=out_features, learning_rate=learning_rate)

logger = TensorBoardLogger(save_dir=log_dir, name=experiment_name)

checkpoint_callback = ModelCheckpoint(
    monitor="Validation Loss", dirpath=experiment_dir, save_top_k=1, save_last=True, every_n_train_steps = 0, every_n_epochs=10
)

trainer_args = {'gpus': 1, 'min_epochs': 1, 'max_epochs': 1000, 'benchmark': False, 'log_every_n_steps':10,
                'callbacks': [
                    checkpoint_callback, 
                    ], 
                'logger': logger, 
                # 'overfit_batches': 1,
                'default_root_dir': experiment_dir}

trainer = pl.Trainer(fast_dev_run=False, **trainer_args)
trainer.fit(model, train_dataloaders=training_loader, val_dataloaders=validation_loader)
