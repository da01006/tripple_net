import os
import collections
import torch
import numpy as np
import pytorch_lightning as pl
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms
from kornia import augmentation as K
from kornia.augmentation import AugmentationSequential

from utils.img_util import HyperImageUtility


class HyperImageDataSet(Dataset):
    def __init__(self, root_dir:str = "./", is_train:bool=True, apply_augmentation:bool = False, 
                channel:int = 88, height:int = 128, width:int =128,step_size:int=10, num_patches=100):
        super().__init__()
        self.root_dir = root_dir 
        self.split = "train" if is_train else "val"
        self.apply_augmentation = apply_augmentation
        self.height = height
        self.width = width
        self.channel = channel
        self.step_size=step_size
        self.num_patches = num_patches

        # self.transforms = transforms.Compose(
        #     [transforms.ToTensor(),
        #     transforms.Normalize((0.1307,), (0.3081,))]
        # )

        self.data_dir = os.path.join(self.root_dir, self.split)
        self.file_list = [os.path.join(self.data_dir, file) for file in os.listdir(self.data_dir)]

        self.files = collections.defaultdict(list)
        for img in self.file_list:
            self.files[self.split].append(
                {
                    "imgs": img
                }
            )

        
    def __len__(self):
        return len(self.files[self.split])

    def __getitem__(self, index):
        img_path = self.files[self.split][index]['imgs']
        # print(f'img path: {img_path}')
        img_util = HyperImageUtility()
        img_data = img_util.read_file(img_path)

        patch_size = (self.channel, self.height, self.width)
        patches_a = img_util.create_patches(data=img_data, patch_size=patch_size, step_size=self.step_size, num_patches=self.num_patches)
        patches_a_hat = patches_a.copy()
        # print(f'patches shape: {patches_a.shape} ')
        if self.apply_augmentation:
            patches_a_hat = torch.from_numpy(patches_a_hat).float()
            patches_a_hat = self.kornia_augmentation()(patches_a_hat)
            patches_a_hat = patches_a_hat.numpy()

        return img_data, patches_a, patches_a_hat

    def kornia_augmentation(self):
        aug_list = AugmentationSequential(
            K.RandomElasticTransform(kernel_size=(63, 63), sigma=(32.0, 32.0), alpha=(1.0, 1.0), 
                                    align_corners=False, mode='bilinear', padding_mode='zeros', p=0.5),
            K.RandomGaussianBlur(kernel_size=(7,7), sigma=(0.1, 2.0), p=0.5),
            K.RandomVerticalFlip(p=0.5),
            K.RandomHorizontalFlip(p=0.5),
            # K.RandomChannelShuffle(p=0.5),
            K.RandomGaussianNoise(mean=0.0, std=1.0, p=0.5),

            # K.RandomPerspective(0.5, p=1.0),
            same_on_batch=True,
        )

        return aug_list


class HyperImageDataSet2(Dataset):
    def __init__(self, root_dir:str = "./", is_train:bool=True, apply_augmentation:bool = False, 
                channel:int = 88, height:int = 128, width:int =128,step_size:int=10, num_patches=100):
        super().__init__()
        self.root_dir = root_dir 
        self.split = "train" if is_train else "val"
        self.apply_augmentation = apply_augmentation
        self.height = height
        self.width = width
        self.channel = channel
        self.step_size=step_size
        self.num_patches = num_patches

        # self.transforms = transforms.Compose(
        #     [transforms.ToTensor(),
        #     transforms.Normalize((0.1307,), (0.3081,))]
        # )

        self.data_dir = os.path.join(self.root_dir, self.split)
        self.file_list = [os.path.join(self.data_dir, file) for file in os.listdir(self.data_dir)]

        self.files = collections.defaultdict(list)
        for img in self.file_list:
            self.files[self.split].append(
                {
                    "imgs": img
                }
            )

        
    def __len__(self):
        return len(self.files[self.split])

    def __getitem__(self, index):
        img_path = self.files[self.split][index]['imgs']
        # print(f'img path: {img_path}')
        img_util = HyperImageUtility()
        img_data = img_util.read_file(img_path)

        patch_size = (self.channel, self.height, self.width)
        patches_a = img_util.create_patches(data=img_data, patch_size=patch_size, step_size=self.step_size, num_patches=self.num_patches)
        patches_a_hat = patches_a.copy()
        # print(f'patches shape: {patches_a.shape} ')
        if self.apply_augmentation:
            patches_a_hat = torch.from_numpy(patches_a_hat).float()
            patches_a_hat = self.kornia_augmentation()(patches_a_hat)
            patches_a_hat = patches_a_hat.numpy()

        return img_data, img_path, patches_a, patches_a_hat

    def kornia_augmentation(self):
        aug_list = AugmentationSequential(
            # K.RandomElasticTransform(kernel_size=(63, 63), sigma=(32.0, 32.0), alpha=(1.0, 1.0), 
            #                         align_corners=False, mode='bilinear', padding_mode='zeros', p=0.5),
            K.RandomGaussianBlur(kernel_size=(9,9), sigma=(0.1, 2.0), p=0.5),
            # K.RandomVerticalFlip(p=0.5),
            # K.RandomHorizontalFlip(p=0.5),
            # # K.RandomChannelShuffle(p=0.5),
            # K.RandomGaussianNoise(mean=0.0, std=1.0, p=0.5),

            # K.RandomPerspective(0.5, p=1.0),
            same_on_batch=True,
        )

        return aug_list
        
          
    
if __name__ == "__main__":
    # export PYTHONPATH='/vol/research/RobotFarming/Projects/tripplet_net'
    import torchvision
    data_dir = r'/vol/research/RobotFarming/Projects/data/full'
    enmap = HyperImageDataSet(root_dir=data_dir, is_train=True, apply_augmentation=True)
    _, a, a_hat = enmap.__getitem__(2) # next(iter(enmap))#
    # breakpoint()
    print(a.shape)
    img_util = HyperImageUtility()
    img_data = img_util.move_axis(a[5])
    img_data = img_util.extract_percentile_range(img_data, 2, 98)
    rgb_img = img_util.extract_rgb(data=img_data, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))

    img_data2 = img_util.move_axis(a_hat[5])
    img_data2 = img_util.extract_percentile_range(img_data2, 2, 98)
    rgb_img2 = img_util.extract_rgb(data=img_data2, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))

    img_data3 = img_util.move_axis(a_hat[1])
    img_data3 = img_util.extract_percentile_range(img_data3, 2, 98)
    rgb_img3 = img_util.extract_rgb(data=img_data3, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))

    # imgs = [torch.from_numpy(rgb_img), torch.from_numpy(rgb_img_a_hat)]
    # imgs = torchvision.utils.make_grid(imgs)
    img_util.display_image_1x3(rgb_img, rgb_img2, rgb_img3)

    # print(next(iter(enmap)))