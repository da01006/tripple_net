import os
import collections
from pathlib import Path

import torch
import numpy as np
import pytorch_lightning as pl
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms
from kornia import augmentation as K
from kornia.augmentation import AugmentationSequential

from utils.img_util import HyperImageUtility
from utils.patcher import ImagePatcher

class HyperImagePatcherDataSet(Dataset):

    def __init__(
            self,
            root_dir,
            patch_height: int = 500,
            patch_width: int = 500,
            overlap_height_ratio: float = 0.0,
            overlap_width_ratio: float = 0.0,
            num_channels: int = 88,
            apply_augmentation:bool = False,
            is_train:bool=True,
            save_patchDF_toCSV: bool = False

        ):
        super().__init__()
        self.root_dir = root_dir
        self.patch_height=patch_height
        self.patch_width=patch_width
        self.overlap_height_ratio=overlap_height_ratio
        self.overlap_width_ratio=overlap_width_ratio
        self.num_channels=num_channels
        self.apply_augmentation=apply_augmentation
        self.split = "train" if is_train else "val"

        self.image_data_dir = os.path.join(self.root_dir, self.split)
        self.file_names = [os.path.join(self.image_data_dir, fname) for fname in os.listdir(self.image_data_dir)]
        # data_path = Path(self.root_dir)
        # self.image_utility = HyperImageUtility()
        self.image_patcher = ImagePatcher(data_path=self.image_data_dir)
        self.patch_df = self.image_patcher.create_image_slices_df( 
                                        file_names=self.file_names,
                                        slice_height=self.patch_height,
                                        slice_width=self.patch_width,
                                        overlap_height_ratio=self.overlap_height_ratio,
                                        overlap_width_ratio=self.overlap_width_ratio
                                    )

    def __len__(self):
        return len(self.patch_df)

    
    def __getitem__(self, index):
        row = self.patch_df.iloc[index]
        bbox = list(row[["xmin", "ymin", "xmax", "ymax"]].values)

        img_path = os.path.join(self.image_data_dir, row.file_name)
        img = [x for x in self.image_patcher.images if img_path in x.keys()]
        img = img[0][img_path]
        # img = self.image_utility.read_file(img_path)

        patch = self.image_patcher.get_patch_from_bounding_box(img, bbox, self.num_channels)
        positive = patch.detach().clone() # copies the patch tensor to +ve
        
        if self.apply_augmentation:
            positive = self._kornia_augmentation()(positive)
            # positive = positive.numpy().squeeze()

        return patch, positive
    

    def _kornia_augmentation(self):
        aug_list = AugmentationSequential(
            # K.RandomElasticTransform(kernel_size=(63, 63), sigma=(32.0, 32.0), alpha=(1.0, 1.0), 
            #                         align_corners=False, mode='bilinear', padding_mode='zeros', p=0.3),
            K.RandomGaussianBlur(kernel_size=(9,9), sigma=(0.1, 5.0), p=.5), # use 100%
            # K.RandomVerticalFlip(p=0.5),
            # K.RandomHorizontalFlip(p=0.5),
            # K.RandomChannelShuffle(p=0.5),
            # K.RandomGaussianNoise(mean=0.0, std=1.0, p=0.3),
            same_on_batch=True,
        )

        return aug_list


if __name__ == "__main__":
    # export PYTHONPATH='/vol/research/RobotFarming/Projects/tripplet_net'
    img_root_dir = r'/vol/research/RobotFarming/Projects/data/full'
    num_channels: int = 88


    print("Loading Dataset")
    hip = HyperImagePatcherDataSet(img_root_dir, patch_height=160, patch_width=160, overlap_height_ratio=0.8, overlap_width_ratio=0.8, apply_augmentation=True, is_train=False)
    # print(hip.patch_df.head(20))
    # print(len(hip.image_patcher.images))
    
    # for idx in range(2):
    #     patch, positive = hip.__getitem__(idx)
    #     print(patch.shape)
    #     patch = hip.image_utility.move_axis(patch)
    #     patch = hip.image_utility.extract_percentile_range(patch, 2, 98)
    #     rgb_img = hip.image_utility.extract_rgb(data=patch, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))

    #     print(positive.shape)
    #     positive = hip.image_utility.move_axis(positive)
    #     positive = hip.image_utility.extract_percentile_range(positive, 2, 98)
    #     positive_rgb_img = hip.image_utility.extract_rgb(data=positive, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
    #     hip.image_utility.display_image_1x3(rgb_img, positive_rgb_img, rgb_img)

    from torch.utils.data import DataLoader
    print("Loaded Images with {} patches".format(len(hip)))
    training_loader = DataLoader(hip, batch_size=2, num_workers=0)
    import time

    start = time.time()
    print("hello")
    for data in training_loader:
        a, p = data
        a = a.to("cuda:0")
        p = p.to("cuda:0")
        # print(type(a))
        # print(type(p))
    
        # print(a.is_cuda)
        
    
    end = time.time()
    print(f'total time {end - start}')