import os
import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.data import DataLoader

# from model.vgg import Vgg16Net
from model.vgg_pl import Vgg16Main
from dataloaders.hyper_dataset import HyperImageDataSet2
from utils.img_util import HyperImageUtility

# query_path = r'/vol/research/RobotFarming/Projects/tripplet_net/img_patches/val/raw_img_s20_feature_fext3_man_20221130_175210_5/raw_patch_s20_fext3_man_20221130_175210_5_43x88x160x160_0.tiff'
feature_path = r'/vol/research/RobotFarming/Projects/tripplet_net/img_patches/val/features/img_s20_feature_fext3_man_20221130_175210_5_43x4096.pt'
# model_path = r"/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor/model_s256_20221115_121134_86.pth"
# model_path = r"/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor/model_hw160_o005_b10_fext3_man_20221130_175210_5.pth"
model_path = r"/vol/research/RobotFarming/Projects/tripplet_net/experiments/condor/model_hw160_o005_b10_fext3_man_20221130_175210_2.pth"
raw_image_root = r'/vol/research/RobotFarming/Projects/tripplet_net/img_patches/val/augmented_img_s20_feature_fext3_man_20221130_175210_5'

# Load model
# model = Vgg16Net(in_channels=88, num_classes=32)
model = Vgg16Main()
model.load_state_dict(torch.load(model_path))
# model.classifier = nn.Sequential(*list(model.classifier.children())[:-3])

for param in model.parameters():
    param.requires_grad = False

model.eval()

fs = os.listdir(raw_image_root)
top_k = 1
scores = []

for file_name in fs:
    fn_path = os.path.join(raw_image_root, file_name)
    img_util = HyperImageUtility()
    query = img_util.read_file(fn_path)
    query = torch.from_numpy(query).unsqueeze(0)
    database = torch.load(feature_path)
    # print(query.shape)
    # print(type(database))
    fn_num = int(file_name.split('_')[-1].split('.')[0])
    # print(f'fnum: {fn_num}')
    with torch.no_grad():  
        q_output = model(query.float())
        q_output = F.normalize(q_output)
        sim = q_output * database
        sim = sim.sum(1)
        sim_rank = sim.argsort(descending=True)
        top_rank = sim_rank[:top_k]
        score = 1 if fn_num in top_rank else 0
        scores.append(score)
        # print(q_output.shape)
        # print(sim.shape)
        print(f'query file index: {fn_num}, similarity rank list: {sim_rank}')
        # print(q_output)
        # print(scores)
        # print(database[sim_rank[10].item()])
    
accuracy = sum(scores) / len(scores) * 100

print(f'Accuracy: {accuracy:.0f}%')