import os
import torch
import pandas as pd
from functools import partial
from utils.img_util import HyperImageUtility
from multiprocessing import Pool

class ImagePatcher:
    def __init__(self, data_path) -> None:
        self.images_path=data_path
        self.file_names=[os.path.join(data_path, fname) for fname in os.listdir(data_path)]


        self.img_util = HyperImageUtility()

        self.images = []
        for fn in self.file_names:
            self.images.append({fn: torch.from_numpy(self.img_util.read_file(fn)).float()})

    def get_image_sizes_df(self, file_names):
        pool = Pool(processes=len(os.sched_getaffinity(0)))
        image_sizes = pool.map(partial(self.get_image_size), file_names)
        sizes_df = pd.DataFrame(image_sizes)
        return sizes_df


    def get_image_size(self, file_name):
        
        img = [x for x in self.images if file_name in x.keys()]
        img = img[0][file_name]
        c, h, w = img.shape
        return {"file_name": file_name.split('/')[-1], "image_height": h, "image_width": w, "image_channel": c}

    def get_patch_from_bounding_box(self, image, bbox, num_channels):
        x_min, y_min, x_max, y_max = bbox
        patch = image[:num_channels,y_min:y_max, x_min:x_max]
        #print(patch.shape)
        return patch


    def calculate_slice_bboxes(
        self,
        image_height: int,
        image_width: int,
        slice_height: int = 512,
        slice_width: int = 512,
        overlap_height_ratio: float = 0.2,
        overlap_width_ratio: float = 0.2,
    ) -> list[list[int]]:
        """
        Given the height and width of an image, calculates how to divide the image into
        overlapping slices according to the height and width provided. These slices are returned
        as bounding boxes in xyxy format.
        :param image_height: Height of the original image.
        :param image_width: Width of the original image.
        :param slice_height: Height of each slice
        :param slice_width: Width of each slice
        :param overlap_height_ratio: Fractional overlap in height of each slice (e.g. an overlap of 0.2 for a slice of size 100 yields an overlap of 20 pixels)
        :param overlap_width_ratio: Fractional overlap in width of each slice (e.g. an overlap of 0.2 for a slice of size 100 yields an overlap of 20 pixels)
        :return: a list of bounding boxes in xyxy format
        """

        slice_bboxes = []
        y_max = y_min = 0
        y_overlap = int(overlap_height_ratio * slice_height)
        x_overlap = int(overlap_width_ratio * slice_width)
        while y_max < image_height:
            x_min = x_max = 0
            y_max = y_min + slice_height
            while x_max < image_width:
                x_max = x_min + slice_width
                if y_max > image_height or x_max > image_width:
                    xmax = min(image_width, x_max)
                    ymax = min(image_height, y_max)
                    xmin = max(0, xmax - slice_width)
                    ymin = max(0, ymax - slice_height)
                    slice_bboxes.append([xmin, ymin, xmax, ymax])
                else:
                    slice_bboxes.append([x_min, y_min, x_max, y_max])
                x_min = x_max - x_overlap
            y_min = y_max - y_overlap
        return slice_bboxes


    def create_image_slices_df(
        self,
        # images_path,
        file_names,
        slice_height: int = 250,
        slice_width: int = 250,
        overlap_height_ratio: float = 0.0,
        overlap_width_ratio: float = 0.0,
    ):
        sizes_df = self.get_image_sizes_df(
                # images_path, 
                file_names#.file_name.unique()
            )
        sizes_df["slices"] = sizes_df.apply(
            lambda row: self.calculate_slice_bboxes(
                row.image_height,
                row.image_width,
                slice_height,
                slice_width,
                overlap_height_ratio,
                overlap_width_ratio,
            ),
            axis=1,
        )

        slices_row_df = (
            sizes_df[["file_name", "slices"]]
            .explode("slices")
            .rename(columns={"slices": "slice"})
        )

        slices_row_df = pd.DataFrame(
            slices_row_df.slice.tolist(),
            columns=["xmin", "ymin", "xmax", "ymax"],
            index=slices_row_df.file_name,
        ).reset_index()

        image_slices_df = pd.merge(
            slices_row_df,
            sizes_df[["file_name", "image_height", "image_width", "image_channel"]],
            how="inner",
            on="file_name",
        )
        
        image_slices_df.reset_index(inplace=True)
        image_slices_df.rename(columns={"index": "slice_id"}, inplace=True)

        return image_slices_df


if __name__ == "__main__":
    # export PYTHONPATH='/vol/research/RobotFarming/Projects/tripplet_net'
    img_root_dir = r'/vol/research/RobotFarming/Projects/data/full/val'
    fnames = [os.path.join(img_root_dir, fname) for fname in os.listdir(img_root_dir)]
    # data_path = Path(img_root_dir)

    slice_height: int = 258
    slice_width: int = 258
    overlap_height_ratio: float = 0.0
    overlap_width_ratio: float = 0.0
    num_channels: int = 88
    # images_path = data_path/'train_sample'
    # df = get_image_sizes_df(images_path=data_path, file_names=fnames)
    ip = ImagePatcher(img_root_dir)
    df = ip.create_image_slices_df(
        # images_path=data_path, 
        file_names=fnames,
        slice_height=slice_height,
        slice_width=slice_width,
        overlap_height_ratio=overlap_height_ratio,
        overlap_width_ratio=overlap_width_ratio
        )
    print(df.head())
    print(df.shape)

    # for idx in range(32):
    #     row = df.iloc[idx]
    #     #print(row.file_name)
    #     bbox = list(row[["xmin", "ymin", "xmax", "ymax"]].values)
    #     #print(bbox)
    #     img_path = os.path.join(img_root_dir, row.file_name)
    #     img_util = HyperImageUtility()
    #     img = img_util.read_file(img_path)
    #     print(img.shape)
    #     patch = ip.get_patch_from_bounding_box(img, bbox, num_channels)
    #     print(patch.shape)
    #     print('===============')
    #     # patch = img_util.move_axis(patch)
    #     # patch = img_util.extract_percentile_range(patch, 2, 98)
    #     # rgb_img = img_util.extract_rgb(data=patch, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
    #     # img_util.display_image(rgb_img)