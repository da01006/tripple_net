from fileinput import filename
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import random
import os
import rasterio as rio
from rasterio.enums import Resampling
from rasterio.plot import show
from patchify import patchify
from PIL import Image
from skimage import io

class HyperImageUtility:
    def __init__(self, 
        # data_dir, 
        # file_name
    ) -> None:
        # self.data_dir = data_dir
        # self.file_name = file_name
        # self.absolute_filename = os.path.join(data_dir, file_name)
        pass

    def extract_rgb(self, data, r_range:tuple, g_range:tuple, b_range:tuple) -> np.ndarray:
        # print(f'extract_rgb - data shape:: {data.shape}')
        r_mean = np.mean(data[:,:,r_range[0]:r_range[-1]], axis=2)
        g_mean = np.mean(data[:,:,g_range[0]:g_range[-1]], axis=2)
        b_mean = np.mean(data[:,:,b_range[0]:b_range[-1]], axis=2)

        rgb_img = np.zeros((data.shape[0], data.shape[1], 3))

        rgb_img[:,:,0]=r_mean
        rgb_img[:,:,1]=g_mean
        rgb_img[:,:,2]=b_mean
        
        # rgb_img = (rgb_img - np.min(rgb_img))/np.ptp(rgb_img)
        # print(f'After: {np.max(rgb_img)}')
        return rgb_img

    def extract_percentile_range(self, data, lo, hi):
        data = (data - np.percentile(data, lo))/np.percentile(data, hi)
        return data

    def display_image(self, image, save_image=False, path=None, fname='rgb_color') -> None:
        plt.figure(figsize=(20, 15))
        plt.axis('off')
        plt.imshow(image)
        plt.show()
        if save_image:
            if path:
                fname = os.path.join(path, fname)
            plt.savefig(f'{fname}.png')

    def display_image_1x3(self, img1, img2, img3) -> None:
        fig = plt.figure(figsize=(12, 8))
        ax = fig.add_subplot(1, 3, 1)
        imgplot = plt.imshow(img1)
        ax.set_title('Anchor')
        ax.set_axis_off()
        ax = fig.add_subplot(1, 3, 2)
        imgplot = plt.imshow(img2)
        ax.set_title('Positive')
        ax.set_axis_off()
        ax = fig.add_subplot(1, 3, 3)
        imgplot = plt.imshow(img3)
        ax.set_title('Negative')
        ax.set_axis_off()
        plt.show()

    def ensure_dir(self, file_path):
        # directory = os.path.dirname(file_path)

        if not os.path.exists(file_path):
            os.makedirs(file_path)

    def read_file(self, file_path) -> np.ndarray:
            img = rio.open(file_path) 
            data = img.read()
            # print(f'size of file read: {data.shape}, dtype: {data.dtype}')
            return data.astype('int16')

    def move_axis(self, data, undo:bool=False):
        if undo:
            data = np.moveaxis(data, (1, 0), (2, 1))
        else:
            data = np.moveaxis(data, 0, -1)
        
        return data

    def generate_train_test_split(self, data, fraction:float =0.2):
        assert fraction >= 0.0 and fraction <= 1.0, 'The fraction must be between 0.0 and 1.0'
        random.seed(1)
        random.shuffle(data)
        stop_index = round(len(data) * fraction)
        test, train = data[:stop_index], data[stop_index:]
        print(f'Test - {len(test)}, Train - {len(train)}')
        return train, test

    def save_file_names(self, data, path=None):
        for name, entries in data.items():
            if path:
                name = os.path.join(path, name)

            with open(f'{name}.txt', 'w') as fp: 
                for item in entries:
                    fp.write("%s\n" % item)

        print('Done!')

    def create_patches(self, data, patch_size:tuple, step_size:int, num_patches:int):
        img_patches = patchify(data, patch_size, step=step_size)
        img_patches = img_patches.squeeze()#.astype(np.float64)
        # print(f'img_patches shape => {img_patches.shape}, dtype: {img_patches.dtype}, len: {len(img_patches.shape)}')
        patch_len = len(img_patches.shape)
        if patch_len > 3:
            h = img_patches.shape[-2]
            w = img_patches.shape[-1]
            c = img_patches.shape[-3]
            b = 1
            if patch_len >= 4:
                img_patches = img_patches[0]
                if img_patches.shape[0] * img_patches.shape[1] > 1000 and len(img_patches.shape) >4:
                    img_patches = img_patches[0]
                # print(f'new img_patches shape => {img_patches.shape}, dtype: {img_patches.dtype}, len: {len(img_patches.shape)}')

            for i in range(len(img_patches.shape) - 3):
                b *= img_patches.shape[i]
                
            img_patches = img_patches.reshape(b, c, h, w)

            if b >= num_patches:
                b = num_patches
            else:
                print(f'Requested number of patches({num_patches}) can not be obtained from created patches of size {img_patches.shape}. Returning {b} patches')
            # img_patches = img_patches[0:3,:,:,:]
        # torch.from_numpy(anchor.astype(float))
        return img_patches[0:b,:,:,:]

    def save_image_patches(self, img_patches, path:str='/img_patches', name:str='enmap', own_filename=None):
        # self.ensure_dir(path)
        file_names = []
        for i, image in enumerate(img_patches):
            if own_filename:
                file_name = f'{own_filename}_{str(i)}.tiff'
            else:
                file_name = f'{name}_{str(i)}.tiff'
                if path:
                    file_name = os.path.join(path, file_name)
                
                if i == 0:
                    print(f'Patch Image shape => {image.shape}') 
            file_names.append(file_name)
            print(f'file name {file_name}')
            with rio.open(file_name, 'w', driver='GTiff', height = image.shape[1], width = image.shape[2],
                            count=image.shape[0], dtype=str(image.dtype)) as dst:
                dst.write(image)
        return file_names

if __name__ == "__main__":
    # enmap = HyperImageUtility()
    # l1b_arc_path = r'/vol/research/rssfeed/pytorch_impl/datasets/L1B_Arcachon_3/ENMAP01-____L1B-DT000400126_20170218T110119Z_003_V000204_20200508T124425Z-SPECTRAL_IMAGE_VNIR.TIF'
    # l1b_alps_path = r'/vol/research/rssfeed/pytorch_impl/datasets/L1B_Alps_3/ENMAP01-____L1B-DT000326721_20170626T102029Z_003_V000204_20200407T005820Z-SPECTRAL_IMAGE_VNIR.TIF'
    
    # img_data = enmap.read_file(l1b_arc_path)
    # img_data = enmap.move_axis(img_data)
    # img_data = enmap.extract_percentile_range(img_data, 2, 98)
    # rgb_img = enmap.extract_rgb(data=img_data, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
    # enmap.display_image(rgb_img)

    # creating image patches
    # patch_path = r'/vol/research/RobotFarming/Projects/data/L1B_Arcachon_3_s128'
    # print(f'data shape: {img_data.shape} ')
    # hw = 160
    # c = img_data.shape[0]
    # patch_size = (c, hw, hw)
    # patches = enmap.create_patches(data=img_data, patch_size=patch_size, step_size=128)
    # print(f'patches shape: {patches.shape} ')

    # view a single patch before saving
    # rgb_img = enmap.move_axis(patches[0])
    # rgb_img = enmap.extract_rgb(data=rgb_img, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
    # rgb_img = enmap.extract_percentile_range(rgb_img, 2, 98)
    # enmap.display_image(rgb_img)

    # save image patches
    # fn = enmap.save_image_patches(patches, path=patch_path, name='L1B_Arcachon_3_s128') # sN, where N = step_size
    # train, test = enmap.generate_train_test_split(fn, 0.2)
    # name_dict = {'train': train, 'test': test}
    # enmap.save_file_names(name_dict, patch_path)
    # print(fn)

    # view some patches
    # patch = r'/vol/research/RobotFarming/Projects/data/L1B_Arcachon_3_s128/L1B_Arcachon_3_s128_30.tiff'
    # img_data = enmap.read_file(patch)
    # img_data = enmap.move_axis(img_data)
    # img_data = enmap.extract_percentile_range(img_data, 2, 98)
    # rgb_img = enmap.extract_rgb(data=img_data, r_range=(46, 48), g_range=(23, 25), b_range=(8, 10))
    # enmap.display_image(rgb_img)

    pass